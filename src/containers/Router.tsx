import { Switch, Route, Redirect } from 'react-router-dom';
import { Login, Dashboard } from './pages';

export default function RouterComponent() {
	return (
		<Switch>
			<Route exact path="/login" component={Login} />
			<Route exact path="/dashboard" component={Dashboard} />

			<Redirect to="/login" />
		</Switch>
	);
}
