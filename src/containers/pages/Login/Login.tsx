import { useEffect, useState } from 'react';

import Cookies from 'js-cookie';
import { History } from 'history';
import LoginForm from './LoginForm/LoginForm';
import { ILoginProps, ILoginResponseProps } from '../../../hooks/useOnLogin';
import { useOnLogin } from '../../../hooks';

export default function Login({ history }: History) {
	const [loginData, setLoginData] = useState<ILoginProps>({
		username: '',
		password: '',
	});

	const { success, errorMessage }: ILoginResponseProps = useOnLogin(loginData);

	useEffect(() => {
		if (Cookies.get('token')) history.push('/dashboard');
	}, [history]);

	useEffect(() => {
		if (success) history.push('/dashboard');
	}, [success, history]);

	return <LoginForm errorMessage={errorMessage} handleLogin={(e) => setLoginData(e)} />;
}
