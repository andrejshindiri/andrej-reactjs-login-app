import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { Container, Row, Col, FormGroup, Button } from 'reactstrap';
import { Input } from '../../../../components/Form';
import { ILoginProps } from '../../../../hooks/useOnLogin';

import classes from './LoginForm.module.scss';
import logo from '../../../../logo.svg';

export interface ILoginFormProps {
	handleLogin(data: any): void;
	errorMessage: string;
}

const initialValues: ILoginProps = {
	username: '',
	password: '',
};

const loginSchema = Yup.object({
	username: Yup.string().required('Username is Required!'),
	password: Yup.string().required('Password is Required!'),
});

export default function LoginForm({ handleLogin, errorMessage }: ILoginFormProps) {
	return (
		<div>
			<Formik
				initialValues={initialValues}
				validationSchema={loginSchema}
				onSubmit={(values) => {
					handleLogin(values);
				}}
			>
				{({ ...values }: ILoginProps) => (
					<Container>
						<Row>
							<Col md="12">
								<div className={classes.modal}>
									<div className="w-100 text-center">
										<img className={classes.logo} src={logo} alt="logo" />
									</div>

									<Form data-testid="form">
										<FormGroup>
											<div className="row">
												<label className="col-sm-2 col-form-label">Username</label>
												<div className="col-sm-10">
													<Input
														data-testid="test-username"
														type="text"
														id="username"
														name="username"
														value={values.username}
														placeholder="Username"
													/>
												</div>
											</div>
										</FormGroup>
										<FormGroup>
											<div className="row">
												<label className="col-sm-2 col-form-label">Password</label>
												<div className="col-sm-10">
													<Input
														data-testid="test-password"
														type="password"
														id="password"
														name="password"
														value={values.password}
														placeholder="Password"
													/>
												</div>
											</div>
										</FormGroup>
										{errorMessage && <div className="alert alert-danger">{errorMessage}</div>}

										<div className="text-center">
											<Button data-testid="test-button" color="primary" role="button" type="submit">
												Sign In
											</Button>
										</div>
									</Form>
								</div>
							</Col>
						</Row>
					</Container>
				)}
			</Formik>
		</div>
	);
}
