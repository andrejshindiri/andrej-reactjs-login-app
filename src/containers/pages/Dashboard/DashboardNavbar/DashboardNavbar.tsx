import { useContext, useState } from 'react';

import Cookies from 'js-cookie';
import { IUserContext } from '../../../../store/Store';
import { Collapse, Navbar, NavbarBrand, Nav, NavItem, NavLink, NavbarText, NavbarToggler } from 'reactstrap';
import { UserContext } from '../../../../store/Store';

import logo from '../../../../logo.svg';

export default function DashboardNavbar() {
	const [menuToggle, setMenuToggle] = useState<boolean>(false);

	const user = useContext<IUserContext>(UserContext);

	const toggleHandler = () => setMenuToggle((p) => !p);

	const logout = () => {
		Cookies.remove('token');
	};

	return (
		<Navbar color="light" light expand="md">
			<NavbarBrand href="/" className="mr-auto">
				<img src={logo} alt="Logo" width={50} height={50} />
			</NavbarBrand>
			<NavbarText>
				{user.firstName} {user.lastName}
			</NavbarText>

			<NavbarToggler onClick={toggleHandler} />

			<Collapse className="justify-content-end" isOpen={menuToggle} navbar>
				<Nav navbar>
					<NavItem onClick={logout} role="logout-btn">
						<NavLink href="/">Logout</NavLink>
					</NavItem>
				</Nav>
			</Collapse>
		</Navbar>
	);
}
