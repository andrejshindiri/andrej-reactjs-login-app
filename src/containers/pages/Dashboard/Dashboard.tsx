import { useEffect, useState } from 'react';

import Cookies from 'js-cookie';
import { History } from 'history';
import { IUserContext } from '../../../store/Store';
import axios from '../../../api/AxiosInstance';
import { UserContext } from '../../../store/Store';
import DashboardNavbar from './DashboardNavbar/DashboardNavbar';

export default function Dashboard({ history }: History) {
	const [user, setUser] = useState<IUserContext>({
		firstName: '',
		lastName: '',
	});

	useEffect(() => {
		if (!Cookies.get('token')) history.push('/login');
	}, [history]);

	useEffect(() => {
		const { firstName, lastName } = user;
		if (!firstName || !lastName) {
			getUser();
		}
	}, [user]);

	const getUser = async () => {
		try {
			const { data } = await axios.get('/user');

			if (data) {
				setUser(data);
			}
		} catch (err: any) {
			console.log(err);
		}
	};

	return (
		<UserContext.Provider value={user}>
			<DashboardNavbar />
		</UserContext.Provider>
	);
}
