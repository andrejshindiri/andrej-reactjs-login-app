import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';

import { Dashboard } from '../containers/pages';
import LoginForm from '../containers/pages/Login/LoginForm/LoginForm';
import Router from './TestingRouter';

describe('Testing | Authorization', () => {
	test('should allow login', async () => {
		const mockFn = jest.fn();
		const { getByRole, getByTestId } = render(<LoginForm handleLogin={mockFn} errorMessage="" />);

		const username = getByTestId('test-username');
		const password = getByTestId('test-password');
		const button = getByRole('button');

		expect(username).toBeInTheDocument();
		expect(password).toBeInTheDocument();
		expect(button).toBeInTheDocument();

		fireEvent.change(username, { target: { value: 'admin' } });
		fireEvent.change(password, { target: { value: 'password' } });
		userEvent.click(button);

		await waitFor(() => {
			expect(mockFn).toHaveBeenCalledWith({
				username: 'admin',
				password: 'password',
			});
		});
	});

	test('should allow logout', async () => {
		const mockFn = jest.fn();
		const { getByRole, getByTestId } = render(<LoginForm handleLogin={mockFn} errorMessage="" />);

		const username = getByTestId('test-username');
		const password = getByTestId('test-password');
		const button = getByRole('button');

		expect(username).toBeInTheDocument();
		expect(password).toBeInTheDocument();
		expect(button).toBeInTheDocument();

		fireEvent.change(username, { target: { value: 'admin' } });
		fireEvent.change(password, { target: { value: 'password' } });
		userEvent.click(button);

		await waitFor(() => {
			expect(mockFn).toHaveBeenCalledWith({
				username: 'admin',
				password: 'password',
			});
		});

		Object.defineProperty(window.document, 'cookie', {
			writable: true,
			value: 'token=random-token',
		});

		const dashboard = render(
			<Router RedirectedComponent={() => <Dashboard history={''} />} redirectionUrl="/dashboard" />
		);

		const logoutButton = dashboard.getByRole('logout-btn');

		expect(logoutButton).toBeInTheDocument();

		fireEvent.click(logoutButton);
	});
});
