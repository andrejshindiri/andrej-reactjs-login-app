import { BrowserRouter } from 'react-router-dom';
import Router from './containers/Router';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

export default function App() {
	return (
		<BrowserRouter>
			<div className="App">
				<Router />
			</div>
		</BrowserRouter>
	);
}
