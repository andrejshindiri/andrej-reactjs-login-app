import { memo } from 'react';

import NonMemoInput from './Input/Input';

const Input = memo(NonMemoInput);

export { Input };
