import { useMemo } from 'react';

import { useField, ErrorMessage } from 'formik';

export default function Input(props) {
	const [field, meta] = useField(props);

	const hasError = useMemo(() => meta.error && meta.touched, [meta.error, meta.touched]);

	return (
		<div>
			<input className={`form-control${hasError ? ' is-invalid' : ''}`} {...field} {...props} />
			<ErrorMessage className="text-danger form-text fw-bold" component="div" name={field.name} />
		</div>
	);
}
